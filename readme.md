#screenshot-capture-api
----------------------------------
Loads the WebPage and takes screenshot of whole page in a single file and returns it.

##Built With
Maven : 
	Run as mvn clean install when running through IDE.
	mvn clean install when running through command line.

##How to setup locally
	1. Clone/Download the project "git clone 		https://rachna1505@bitbucket.org/rachna1505/website-snapshot-capture.git"
	
## Running Application local through IDE
	1.Right Clock on project
	2.Click on Run As  Java Application OR Spring Boot App 
	3.Select Main Class ScreenshotAssignmentApplication.java for running the application with embedded 	  	  tomcat.
	
##Invoking an API locally
POST API Endpoint : http://localhost:8080//screenshot/weburl

Sample Request:
{
	"url":"https://filehippo.com"
}

Content-Type : application/json

Sample Output in Response:
Response : byte[]
Content-Type : image/jpeg

Note: In this application we are storing response at "src/main/resources/captureimg/" path which can be used to cross verify the above API response.
The Storage path is configurable, which can be changed via application.properties file