package com.assignment.ScreenshotAssignment.serviceImpl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import com.assignment.ScreenshotAssignment.exception.ScreenshotApplicationError;
import com.assignment.ScreenshotAssignment.service.ScreenshotService;

@Service
public class ScreenshotServiceImpl implements ScreenshotService {
	
	private static final Logger _log = LoggerFactory.getLogger(ScreenshotServiceImpl.class);
	
	@Value("${chrome.driver.name}")
	private String webDriverPropertyName;
	@Value("${chrome.driver.exe.path}")
	private String driverPath;
	@Value("${captured.file.path}")
	private String screenshotPath;

	@Override
	public InputStream getScreenshotResponse(String url) {
		WebDriver webDriver=null;
		try {
			System.setProperty(webDriverPropertyName,
					driverPath);
			
			webDriver = new ChromeDriver();
			webDriver.get(url);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				_log.error("Exception in scrolling down the page", e);
				throw new ScreenshotApplicationError();
			}
			
			return takeScreenshotFromChrome(webDriver,url);
	
		} catch (Exception e1) {
			_log.error("Unable to take a screenshot", e1);
			throw new ScreenshotApplicationError();
		} finally{
			if(webDriver != null)
			webDriver.close();
		}
				
	}

	/**
	 * @param webDriver
	 * @param siteUrl
	 * @return
	 * @throws IOException
	 */
	private InputStream takeScreenshotFromChrome(WebDriver webDriver,String siteUrl) throws IOException {
		Screenshot screenshot = new AShot().shootingStrategy(
				ShootingStrategies.viewportPasting(1000)).takeScreenshot(
				webDriver);
	    
		// saving to the project folder
	    savingImgToFolder(siteUrl, screenshot);
	    
	    //Returning bytes to the http response
	    ByteArrayOutputStream os = new ByteArrayOutputStream();
	    ImageIO.write(screenshot.getImage(), "jpg", os);
	    
	    InputStream input = new ByteArrayInputStream(os.toByteArray());
		return input;
	}

	/**
	 * @param siteUrl 
	 * @param screenshot 
	 * @throws IOException
	 */
	private void savingImgToFolder(String siteUrl, Screenshot screenshot)
			throws IOException {
		String fileName="";
		if(siteUrl.contains("https://www")){
			fileName = siteUrl.replaceAll("https://www", "").replaceAll("/", "");
		}else{
			fileName = siteUrl.replaceAll("https://", "").replaceAll("/", "");
		}
		
	    File file = new File(screenshotPath+fileName+"_screenshot.jpg");
	    ImageIO.write(screenshot.getImage(), "jpg", file);
	}

}
