package com.assignment.ScreenshotAssignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScreenshotAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScreenshotAssignmentApplication.class, args);
	}

}
