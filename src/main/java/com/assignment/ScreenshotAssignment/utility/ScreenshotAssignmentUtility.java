package com.assignment.ScreenshotAssignment.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class ScreenshotAssignmentUtility {

    private static final String urlPattern = "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";

    
    public boolean isValidUrl(String url){
    	Pattern pattern = Pattern.compile(urlPattern);
    	Matcher matcher = pattern.matcher(url);
    	return matcher.matches();
    }
}
