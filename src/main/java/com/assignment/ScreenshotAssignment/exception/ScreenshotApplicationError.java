package com.assignment.ScreenshotAssignment.exception;

public class ScreenshotApplicationError extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ScreenshotApplicationError(){
		super("Screenshot Application error");
	}

	
}
