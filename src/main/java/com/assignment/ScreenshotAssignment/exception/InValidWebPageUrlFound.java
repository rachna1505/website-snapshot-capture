package com.assignment.ScreenshotAssignment.exception;


public class InValidWebPageUrlFound extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InValidWebPageUrlFound(String url){
		super("Invalid Web Page URL :"+url);
	}
	
	
}
