package com.assignment.ScreenshotAssignment.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Centralized Exception handler of the application
 * @author Rachna
 *
 */
@ControllerAdvice
public class ScreenshotExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(InValidWebPageUrlFound.class)
	public ResponseEntity<ScreenshotAssignmentErrorResponse> invalidWebPageUrl(
			Exception ex) {
		ScreenshotAssignmentErrorResponse errorResponse = new ScreenshotAssignmentErrorResponse();
		errorResponse.setError(ex.getMessage());
		errorResponse.setLocalDateTime(LocalDateTime.now());
		errorResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<ScreenshotAssignmentErrorResponse>(
				errorResponse, org.springframework.http.HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ScreenshotApplicationError.class)
	public ResponseEntity<ScreenshotAssignmentErrorResponse> pageScrollIssue(
			Exception ex) {
		ScreenshotAssignmentErrorResponse errorResponse = new ScreenshotAssignmentErrorResponse();
		errorResponse.setError(ex.getMessage());
		errorResponse.setLocalDateTime(LocalDateTime.now());
		errorResponse.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		return new ResponseEntity<ScreenshotAssignmentErrorResponse>(
				errorResponse, org.springframework.http.HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ScreenshotFileConverisonError.class)
	public ResponseEntity<ScreenshotAssignmentErrorResponse> fileConversionError(
			Exception ex) {
		ScreenshotAssignmentErrorResponse errorResponse = new ScreenshotAssignmentErrorResponse();
		errorResponse.setError(ex.getMessage());
		errorResponse.setLocalDateTime(LocalDateTime.now());
		errorResponse.setStatusCode(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
		return new ResponseEntity<ScreenshotAssignmentErrorResponse>(
				errorResponse, org.springframework.http.HttpStatus.BAD_REQUEST);
	}
}
