package com.assignment.ScreenshotAssignment.exception;

/**
 * Custom exceptionto handle file conversion
 * @author Rachna
 *
 */
public class ScreenshotFileConverisonError extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ScreenshotFileConverisonError(){
		super("Error in returning JPEG file");
	}
	
}
