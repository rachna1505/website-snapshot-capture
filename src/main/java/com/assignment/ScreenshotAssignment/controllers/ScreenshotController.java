package com.assignment.ScreenshotAssignment.controllers;

import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.ScreenshotAssignment.exception.InValidWebPageUrlFound;
import com.assignment.ScreenshotAssignment.model.CaptureSite;
import com.assignment.ScreenshotAssignment.service.ScreenshotService;
import com.assignment.ScreenshotAssignment.utility.ScreenshotAssignmentUtility;

@RestController
@RequestMapping("/screenshot")
public class ScreenshotController {

	private static final Logger _log = LoggerFactory
			.getLogger(ScreenshotController.class);

	@Autowired
	private ScreenshotService screenshotService;

	@Autowired
	private ScreenshotAssignmentUtility screenAssignmentUtility;

	@PostMapping(path = "/weburl", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public ResponseEntity<InputStreamResource> processScreenshotRequest(
			@RequestBody CaptureSite site) {

		_log.info("ScreenshotApplication staretd.");
		String url = site.getUrl();
		if (StringUtils.isBlank(url) || !screenAssignmentUtility.isValidUrl(url))
			throw new InValidWebPageUrlFound(url);
		else{
			InputStream input = screenshotService.getScreenshotResponse(url);
			return ResponseEntity.ok()
		            .body(new InputStreamResource(input));
		}
	}

}
