package com.assignment.ScreenshotAssignment.service;

import java.io.InputStream;


public interface ScreenshotService {
	
	public InputStream getScreenshotResponse(String url);
	
}

