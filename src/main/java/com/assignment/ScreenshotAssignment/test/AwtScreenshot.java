package com.assignment.ScreenshotAssignment.test;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class AwtScreenshot {

	public static void main(String[] args) {

		try {
			Thread.sleep(1000);
			Robot r = new Robot();
			
			String path = "E:\\TestJar\\Shot.jpg";
			Rectangle capture =  new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
			BufferedImage image = r.createScreenCapture(capture);
			
			ImageIO.write(image, "jpg", new File(path));
			
		} catch (AWTException | IOException | InterruptedException ex) {
			System.out.println(ex);
		}
	}

}
